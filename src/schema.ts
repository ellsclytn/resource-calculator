export default {
  'advancedEnergyCube': {
    recipe: {
      'enrichedAlloy': 4,
      'energyTablet': 2,
      'osmiumIngot': 2,
      'basicEnergyCube': 1
    }
  },
  'advancedSolarGenerator': {
    recipe: {
      'ironIngot': 3,
      'enrichedAlloy': 2,
      'solarGenerator': 4
    }
  },
  'advancedUniversalCable': {
    recipe: {
      'basicUniversalCable': 8,
      'enrichedAlloy': 1
    }
  },
  'airLockController': {
    recipe: {
      'compressedSteel': 6,
      'compressedMeteoricIron': 2,
      'basicWafer': 1
    }
  },
  'airLockFrame': {
    produces: 4,
    recipe: {
      'compressedAluminum': 6,
      'compressedSteel': 2,
      'oxygenConcentrator': 1
    }
  },
  'airVent': {
    recipe: {
      'compressedTin': 3,
      'compressedSteel': 1
    }
  },
  'atomicAlloy': {
    recipe: {
      'reinforcedAlloy': 1,
      'obsidian': 1,
      'diamond': 1
    }
  },
  'basicUniversalCable': {
    recipe: {
      'steelIngot': 2,
      'redstone': 1
    }
  },
  'basicWafer': {
    produces: 3,
    recipe: {
      'diamond': 1,
      'silicon': 2,
      'redstone': 1,
      'redstoneTorch': 1
    }
  },
  'bed': {
    recipe: {
      'wool': 3,
      'woodPlank': 3
    }
  },
  'beeReceptacle': {
    recipe: {
      'bronzeIngot': 5,
      'glassPane': 1,
      'redstone': 2,
      'weightedPressurePlateLight': 1
    }
  },
  'blockOfSteel': {
    recipe: {
      'steelIngot': 9
    }
  },
  'bronzeGear': {
    recipe: {
      'bronzeIngot': 4,
      'ironIngot': 1
    }
  },
  'carbonRotorBlade': {
    recipe: {
      'rawCarbonMesh': 9
    }
  },
  'climateControlModule': {
    recipe: {
      'bronzeIngot': 6,
      'redstone': 2,
      'bronzeGear': 1
    }
  },
  'compressedAluminum': {
    recipe: {
      'aluminumIngot': 2
    }
  },
  'compressedBronze': {
    recipe: {
      'compressedTin': 1,
      'compressedCopper': 1
    }
  },
  'compressedCopper': {
    recipe: {
      'copperIngot': 2
    }
  },
  'compressedIron': {
    recipe: {
      'ironIngot': 2
    }
  },
  'compressedSteel': {
    recipe: {
      'compressedIron': 1,
      'coal': 2
    }
  },
  'compressedTin': {
    recipe: {
      'tinIngot': 2
    }
  },
  'cryogenicChamber': {
    recipe: {
      'compressedDesh': 6,
      'heavyDutyPlating': 2,
      'bed': 1
    }
  },
  'eliteEnergyCube': {
    recipe: {
      'reinforcedAlloy': 4,
      'goldIngot': 2,
      'energyTablet': 2,
      'advancedEnergyCube': 1
    }
  },
  'energyTablet': {
    recipe: {
      'goldIngot': 3,
      'redstone': 4,
      'enrichedAlloy': 2
    }
  },
  'enrichedAlloy': {
    recipe: {
      'redstone': 1,
      'ironIngot': 1
    }
  },
  'enviromentalProcessor': {
    recipe: {
      'diamond': 2,
      'lapisLazuli': 2,
      'goldenChipset': 1
    }
  },
  'flintAndSteel': {
    recipe: {
      'flint': 1,
      'ironIngot': 1
    }
  },
  'fuelCanister': {
    recipe: {
      'compressedTin': 6,
      'glass': 1,
      'compressedSteel': 1,
      'tinCanister': 1
    }
  },
  'glassPane': {
    produces: 16,
    recipe: {
      'glass': 6
    }
  },
  'goldenChipset': {
    recipe: {
      'redstone': 1,
      'goldIngot': 1
    }
  },
  'goldNugget': {
    produces: 9,
    recipe: {
      'goldIngot': 1
    }
  },
  'heavyDutyPlating': {
    recipe: {
      'compressedSteel': 1,
      'compressedAluminum': 1,
      'compressedBronze': 1
    }
  },
  'heavyNoseCone': {
    recipe: {
      'redstoneTorch': 1,
      'tier3HeavyDutyPlate': 3
    }
  },
  'heavyRocketEngine': {
    recipe: {
      'tier3HeavyDutyPlate': 4,
      'flintAndSteel': 1,
      'stoneButton': 1,
      'tinCanister': 1,
      'airVent': 1
    }
  },
  'heavyRocketFin': {
    recipe: {
      'tier3HeavyDutyPlate': 4,
      'tier2HeavyDutyPlate': 2
    }
  },
  'hopper': {
    recipe: {
      'ironIngot': 5,
      'woodenChest': 1
    }
  },
  'industrialApiary': {
    recipe: {
      'glass': 4,
      'piston': 1,
      'beeReceptacle': 1,
      'sturdyCasing': 1,
      'bronzeGear': 2
    }
  },
  'itemFilter': {
    recipe: {
      'redstone': 4,
      'stick': 4,
      'string': 1
    }
  },
  'kineticGearboxRotor': {
    recipe: {
      'carbonRotorBlade': 4,
      'shaftRefinedIron': 1
    }
  },
  'oceanEmulationUpgrade': {
    recipe: {
      'bronzeGear': 4,
      'climateControlModule': 1,
      'upgradeFrame': 1,
      'enviromentalProcessor': 1,
      'waterBucket': 2
    }
  },
  'osmiumDust': {
    recipe: {
      'osmiumIngot': 1
    }
  },
  'oxygenBubbleDistributor': {
    recipe: {
      'compressedSteel': 4,
      'airVent': 2,
      'compressedAluminum': 1,
      'oxygenFan': 2
    }
  },
  'oxygenCollector': {
    recipe: {
      'compressedSteel': 3,
      'oxygenFan': 1,
      'airVent': 1,
      'tinCanister': 1,
      'compressedAluminum': 2,
      'oxygenConcentrator': 1
    }
  },
  'oxygenCompressor': {
    recipe: {
      'compressedSteel': 4,
      'compressedAluminum': 3,
      'compressedBronze': 1,
      'oxygenConcentrator': 1
    }
  },
  'oxygenConcentrator': {
    recipe: {
      'compressedSteel': 4,
      'compressedTin': 3,
      'tinCanister': 1,
      'airVent': 1
    }
  },
  'oxygenFan': {
    recipe: {
      'compressedSteel': 4,
      'redstone': 1,
      'basicWafer': 1
    }
  },
  'oxygenSealer': {
    recipe: {
      'compressedAluminum': 4,
      'compressedSteel': 2,
      'oxygenFan': 1,
      'airVent': 2
    }
  },
  'piston': {
    recipe: {
      'woodPlank': 3,
      'cobblestone': 4,
      'ironIngot': 1,
      'redstone': 1
    }
  },
  'rawCarbonFibre': {
    recipe: {
      'coalDust': 4
    }
  },
  'rawCarbonMesh': {
    recipe: {
      'rawCarbonFibre': 2
    }
  },
  'redstoneBlock': {
    recipe: {
      'redstone': 9
    }
  },
  'redstoneTorch': {
    recipe: {
      'stick': 1,
      'redstone': 1
    }
  },
  'reinforcedAlloy': {
    recipe: {
      'diamond': 1,
      'enrichedAlloy': 1
    }
  },
  'shaftRefinedIron': {
    recipe: {
      'blockOfSteel': 1
    }
  },
  'solarGenerator': {
    recipe: {
      'solarPanel': 3,
      'enrichedAlloy': 2,
      'osmiumDust': 2,
      'ironIngot': 1,
      'energyTablet': 1
    }
  },
  'solarPanel': {
    recipe: {
      'glassPane': 3,
      'redstone': 2,
      'enrichedAlloy': 1,
      'osmiumIngot': 3
    }
  },
  'spacePack': {
    recipe: {
      'oxygenCollector': 1,
      'oxygenCompressor': 1,
      'oxygenSealer': 2,
      'airLockFrame': 10,
      'airLockController': 2,
      'cryogenicChamber': 2,
      'oxygenBubbleDistributor': 1
    }
  },
  'stick': {
    produces: 4,
    recipe: {
      'woodPlank': 2
    }
  },
  'stoneButton': {
    recipe: {
      'stone': 1
    }
  },
  'sturdyCasing': {
    recipe: {
      'bronzeIngot': 8
    }
  },
  'tier1Booster': {
    recipe: {
      'compressedMeteoricIron': 4,
      'airVent': 1,
      'fuelCanister': 1,
      'heavyDutyPlating': 2
    }
  },
  'tier2HeavyDutyPlate': {
    recipe: {
      'heavyDutyPlating': 1,
      'compressedMeteoricIron': 1
    }
  },
  'tier3HeavyDutyPlate': {
    recipe: {
      'tier2HeavyDutyPlate': 1,
      'compressedDesh': 1
    }
  },
  'tier3Rocket': {
    recipe: {
      'tier3HeavyDutyPlate': 10,
      'heavyNoseCone': 1,
      'heavyRocketEngine': 1,
      'heavyRocketFin': 4,
      'tier1Booster': 2
    }
  },
  'tinCanister': {
    recipe: {
      'tinIngot': 3.5
    }
  },
  'transferNodeItems': {
    recipe: {
      'transferPipe': 1,
      'redstoneBlock': 1,
      'redstone': 2,
      'stone': 2,
      'woodenChest': 1
    }
  },
  'transferPipe': {
    recipe: {
      'stoneSlab': 6,
      'glass': 2,
      'redstone': 1
    }
  },
  'ultimateEnergyCube': {
    recipe: {
      'atomicAlloy': 4,
      'diamond': 2,
      'energyTablet': 2,
      'eliteEnergyCube': 1
    }
  },
  'upgradeFrame': {
    recipe: {
      'tinIngot': 4,
      'goldNugget': 2,
      'redstone': 2
    }
  },
  'weightedPressurePlateLight': {
    recipe: {
      'goldIngot': 2
    }
  },
  'winterEmulationUpgrade': {
    recipe: {
      'bronzeGear': 4,
      'climateControlModule': 1,
      'upgradeFrame': 1,
      'enviromentalProcessor': 1,
      'snow': 2
    }
  },
  'woodenChest': {
    recipe: {
      'woodPlank': 8
    }
  },
  'woodPlank': {
    produces: 4,
    recipe: {
      'wood': 1
    }
  }
}
